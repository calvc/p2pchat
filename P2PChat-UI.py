#!/usr/bin/python3

# Student name and No.: Chan Ka Lok (3035229502)
# Student name and No.: Ho Hilary (3035229306)
# Development platform: Ubuntu 16.10
# Python version:3.5.2
# Version:1

from tkinter import *
import sys
import socket
from threading import Thread, current_thread, RLock
from time import sleep
import re
from queue import Queue

#
# Global variables
#
room_server_sock = None
connected_to_room = False
my_username = None
my_port = None
my_ip = None
my_msg_id = 0
my_id = None

roomname = None

check_membership = False
listening_to_port = False

# lock for protecting link
llock = RLock()

# lock for protecting members
mlock = RLock()

# create queue for inter-thread communication
q = Queue()

# dict for storing members' info
members = {}

threads = []

#list for all sockets
link = []

RECV_GROUP = "G"
RECV_MEMBER = "M"
RECV_PEER = "S"
RECV_TEXT = "T"
RECV_ERROR = "F"


#
# This is the hash function for generating a unique
# Hash ID for each peer.
# Source: http://www.cse.yorku.ca/~oz/hash.html
#
# Concatenate the peer's username, str(IP address),
# and str(Port) to form the input to this hash function
#
def sdbm_hash(instr):
    hash = 0
    for c in instr:
        hash = int(ord(c)) + (hash << 6) + (hash << 16) - hash
    return hash & 0xffffffffffffffff


#
# Functions to handle user input
#


# print debug msg
def debug(name, msg):
        print("[%s %s]: %s" % (current_thread().name, name, msg))

# display msg in CmdWin
def display(msg):
    CmdWin.insert(1.0, msg)
    win.update()

# display msg in MsgWin
def display_msg(msg):
    MsgWin.insert(1.0, msg)
    win.update()



# build request | input type = list
def buildReq(args):
    return (":".join(args) + "::\r\n")


def do_User():
    global my_username
    if connected_to_room:
        display("\nCannot change username after joined a room")
        userentry.delete(0, END)
        return
    if not len(userentry.get()):
        if my_username:
            display("\n[User] Username can not change to empty string")
        else:
            display("\n[User] Username can not be empty")
    else:
        my_username = userentry.get()
        display("\n[User] username: " + my_username)
    userentry.delete(0, END)


def do_List():
    room_server_sock.send("L::\r\n".encode())


def do_Join():
    global roomname
    if connected_to_room:
        display("\n[Group] Already joined another chatroom")
    elif not my_username:
        display("\nPlease register your username")
    elif not len(userentry.get()):
        display("\n[Group] Groupname can not be empty")
    else:
        roomname = userentry.get()
        display("\n[Group] groupname: " + roomname)
        req = buildReq(["J", roomname, my_username, my_ip, my_port]).encode()
        room_server_sock.send(req)
    userentry.delete(0, END)


def do_Send():
    global my_msg_id
    if not connected_to_room:
        display("\nPlease join a room first")
    elif not len(userentry.get()):
        display("\nMessage cannot be empty")
    else:
        msg = userentry.get()
        userentry.delete(0, END)
        my_id = sdbm_hash(my_username + my_ip + my_port)
        req = buildReq([
            "T", roomname, str(my_id), my_username, str(my_msg_id), str(
                len(msg)), msg
        ])
        my_msg_id += 1
        with llock:
            for s in link:
                s.send(req.encode())
        display_msg("\n%s: %s" % (my_username, msg))
    userentry.delete(0, END)


def do_Quit():
    CmdWin.insert(1.0, "\nPress Quit")
    if room_server_sock:
        room_server_sock.close()
    with llock:
        for s in link:
            s.close()
    sys.exit(0)


def connectToRoom():
    global my_ip, room_server_sock
    try:
        debug("ConnectToRoom", "connecting")
        room_server_sock = socket.socket()
        room_server_sock.connect((sys.argv[1], int(sys.argv[2])))
        # get my ip
        my_ip = room_server_sock.getsockname()[0]
        # start listener for room server
        roomServerThread = Thread(target=roomServerListener)
        roomServerThread.daemon = True
        roomServerThread.start()
        threads.append(roomServerThread)
        debug("ConnectToRoom", "connection established")
        display("\nConnected to room server")
    except socket.error:
        debug("ConnectToRoom", "cannot connect to roomserver, retry in 3 second")
        display("\nCannot connect to room server, retry in 3 second")
        sleep(3)
        connectToRoom()


def keepAlive():
    debug("KeepAlive", "Started")
    while True:
        for i in range(20):
            sleep(1)
        debug("KeepAlive", "Keeping alive")
        try:
            req = buildReq(
                ["J", roomname, my_username, my_ip, my_port]).encode()
            room_server_sock.send(req)
        except socket.error:
            debug("KeepAlive", "Socket error")
            connectToRoom()


def handlePeerConnection():
    global listening_to_port, members, my_id
    established = False
    self = sdbm_hash(my_username  + my_ip + my_port)
    my_id = self
    mlist = sorted(list(members))
    mlength = len(mlist)
    i = mlist.index(self) + 1
    if i >= mlength:
        i = (mlist.index(self) + 1) % mlength

    while mlist[i] != self:
        if members[mlist[i]]["conn"]:
            i = (i + 1) % mlength
        else:
            try:
                sock = socket.socket()
                sock.settimeout(5)
                sock.connect(
                    (members[mlist[i]]['ip'], int(members[mlist[i]]['port'])))
                debug("PeerSearch", "peer connection established")

                #init handshake
                req = buildReq([
                    "P", roomname, my_username, my_ip, my_port, str(my_msg_id)
                ]).encode()
                sock.send(req)
                res = sock.recv(4096)
                if res:
                    res = res.decode()
                    target_msg_id = re.findall("S:(\d+)::\r\n", res)
                    if len(target_msg_id) == 1:
                        debug("PeerSearch", "connected")
                        sock.settimeout(None)
                        with mlock:
                            members[mlist[i]]['conn'] = sock
                            members[mlist[i]]['msg_id'] = int(target_msg_id[0])
                        established = True
                        with llock:
                            link.append(sock)
                        listener = Thread(
                            target=peerListener, args=(sock, mlist[i]))
                        listener.daemon = True
                        listener.start()
                        break
                    else:
                        debug("PeerSearch", "error")
                        sock.close()
                        i = (i + 1) % mlength
                else:
                    debug("PeerSearch", "error")
                    sock.close()
                    i = (i + 1) % mlength
            except socket.timeout:
                debug("PeerSearch", "timeout")
                sock.close()
                i = (i + 1) % mlength
            except socket.error:
                debug("PeerSearch", "peer connection error")
                sock.close()
                i = (i + 1) % mlength

    if not established:
        debug("PeerSearch", "cant establish forward link")
    if not listening_to_port:
        listening_to_port = True
        serverThread = Thread(target=server)
        serverThread.daemon = True
        serverThread.start()


def server():
    server = socket.socket()
    server.bind(("", int(my_port)))
    server.listen(1)
    debug("PortListener", "listening")
    while True:
        client, addr = server.accept()
        listener = Thread(target=peerListener, args=(client, ))
        listener.daemon = True
        listener.start()


def peerListener(sock, target_peer=None):
    global check_membership, members
    while True:
        try:
            req = sock.recv(4096)
            if req:
                req = req.decode()
                debug("PeerThread", req)
                if req[0] == "P":
                    check_membership = True
                    room_server_sock.send(
                        buildReq(["J", roomname, my_username, my_ip, my_port])
                        .encode())
                    res = q.get()
                    q.task_done()
                    check_membership = False
                    buildMemberList(res)
                    tmp = re.findall("P:"+roomname+":(\w+):(\d+\.\d+\.\d+\.\d+):(\d+):(\d+)::\r\n",req)[0]
                    id = sdbm_hash(tmp[0]+tmp[1]+tmp[2])
                    if id in members:
                        sock.send(("S:" + str(my_msg_id) + "::\r\n").encode())
                        with mlock:
                            members[id]['conn'] = sock
                            members[id]['msg_id'] = int(tmp[3])
                        debug("PeerThread", "accept connection")
                        with llock:
                            link.append(sock)
                    else:
                        debug("PeerThread", "reject connection")
                elif req[0] == "T":
                    #(origin hid) (origin username) (origin msg id) (length) (content)
                    tmp = re.findall("T:"+roomname+":(\d+):(\w+):(\d+):(\d+):(.*)::\r\n",req)
                    if len(tmp) == 1:
                        tmp = tmp[0]
                        id = int(tmp[0])
                        if not id == my_id:
                            if not id in members :
                                check_membership = True
                                room_server_sock.send(
                                    buildReq(["J", roomname, my_username, my_ip, my_port])
                                    .encode())
                                res = q.get()
                                q.task_done
                                check_membership = False
                                buildMemberList(res)
                            if id in members:
                                if not 'msg_id' in members[id]:
                                    with mlock:
                                        members[id]['msg_id'] = int(tmp[2]) + 1
                                    display_msg("\n"+tmp[1]+": "+tmp[4])
                                    with llock:
                                        for s in link:
                                            s.send(req.encode())
                                elif int(tmp[2]) == members[id]['msg_id']:
                                    with mlock:
                                        members[id]['msg_id'] += 1
                                    display_msg("\n"+tmp[1]+": "+tmp[4])
                                    with llock:
                                        for s in link:
                                            s.send(req.encode())
                                else:
                                    debug("PeerThread", "Discard old T message")
                                debug("PeerThread", str(members[id]['msg_id']) +" | "+ tmp[2])
                            else:
                                debug("PeerThread", "T message not from members")
                        else:
                            debug("PeerThread", "Discard T message from myself")
                    else:
                        debug("PeerThread", "Discard error T message")

            else:
                debug("PeerThread", "connection broken")
                sock.close()
                with llock:
                    link.remove(sock)
                if target_peer:
                    with mlock:
                        del members[target_peer]
                    handlePeerConnection()
                break
        except socket.error:
            debug("PeerThread", "Socket error")

def buildMemberList(d):
    global members
    for m in re.findall("(\w+:\d+\.\d+\.\d+\.\d+:\d+)", d):
        tmp = m.split(":")
        id = sdbm_hash(tmp[0]+tmp[1]+tmp[2])
        with mlock:
            if not id in members:
                members[id] = {}
                members[id]['username'] = tmp[0]
                members[id]['ip'] = tmp[1]
                members[id]['port'] = tmp[2]
                members[id]['conn'] = None


def roomServerListener():
    global connected_to_room
    debug("RoomServerListener", "Thread started. Listening to room server")
    while True:
        try:
            res = room_server_sock.recv(4096)
        except socket.timeout:
            debug("RoomServerListener", 'timeout')
        except:
            debug("RoomServerListener", 'unknown error')
        if res:
            res = res.decode()
            debug("RoomServerListener", "Received MSG: " + res)
            # check the first char in response and act accordingly
            if res[0] == RECV_GROUP:
                if len(res) > 5:
                    for g in res.split(":")[1:-2]:
                        display("\n" + g)
                    display("\nChatroom list:")
                else:
                    display("\nNo chatroom yet")
            elif res[0] == RECV_MEMBER:
                gid = res.split(":")[1]
                # if not joinned a room b4 -> start keep alive thread
                if not connected_to_room:
                    connected_to_room = True
                    keepAliveThread = Thread(target=keepAlive)
                    keepAliveThread.daemon = True
                    keepAliveThread.start()
                    display("\nJoinned room > " + roomname)
                    display("\n" + "\n".join(re.findall(":(\w+):\d+.", res)))
                    display("\n\nMember List")
                    buildMemberList(res)
                    handlePeerConnection()
                elif check_membership:
                    q.put(res)
            elif res[0] == RECV_ERROR:
                debug("RoomServerListener", "Received error msg: " + res)
            else:
                debug("RoomServerListener", "Unknown msg: " + res)
        else:
            debug("RoomServerListener", "Broken connection to the room server. Trying to connect again...")
            room_server_sock.close()
            connectToRoom()
            break


# Set up of Basic UI
#
win = Tk()
win.title("MyP2PChat")

#Top Frame for Message display
topframe = Frame(win, relief=RAISED, borderwidth=1)
topframe.pack(fill=BOTH, expand=True)
topscroll = Scrollbar(topframe)
MsgWin = Text(
    topframe,
    height='15',
    padx=5,
    pady=5,
    fg="red",
    exportselection=0,
    insertofftime=0)
MsgWin.pack(side=LEFT, fill=BOTH, expand=True)
topscroll.pack(side=RIGHT, fill=Y, expand=True)
MsgWin.config(yscrollcommand=topscroll.set)
topscroll.config(command=MsgWin.yview)

#Top Middle Frame for buttons
topmidframe = Frame(win, relief=RAISED, borderwidth=1)
topmidframe.pack(fill=X, expand=True)
Butt01 = Button(
    topmidframe, width='8', relief=RAISED, text="User", command=do_User)
Butt01.pack(side=LEFT, padx=8, pady=8)
Butt02 = Button(
    topmidframe, width='8', relief=RAISED, text="List", command=do_List)
Butt02.pack(side=LEFT, padx=8, pady=8)
Butt03 = Button(
    topmidframe, width='8', relief=RAISED, text="Join", command=do_Join)
Butt03.pack(side=LEFT, padx=8, pady=8)
Butt04 = Button(
    topmidframe, width='8', relief=RAISED, text="Send", command=do_Send)
Butt04.pack(side=LEFT, padx=8, pady=8)
Butt05 = Button(
    topmidframe, width='8', relief=RAISED, text="Quit", command=do_Quit)
Butt05.pack(side=LEFT, padx=8, pady=8)

#Lower Middle Frame for User input
lowmidframe = Frame(win, relief=RAISED, borderwidth=1)
lowmidframe.pack(fill=X, expand=True)
userentry = Entry(lowmidframe, fg="blue")
userentry.pack(fill=X, padx=4, pady=4, expand=True)

#Bottom Frame for displaying action info
bottframe = Frame(win, relief=RAISED, borderwidth=1)
bottframe.pack(fill=BOTH, expand=True)
bottscroll = Scrollbar(bottframe)
CmdWin = Text(
    bottframe, height='15', padx=5, pady=5, exportselection=0, insertofftime=0)
CmdWin.pack(side=LEFT, fill=BOTH, expand=True)
bottscroll.pack(side=RIGHT, fill=Y, expand=True)
CmdWin.config(yscrollcommand=bottscroll.set)
bottscroll.config(command=CmdWin.yview)


def main():
    global my_port
    if len(sys.argv) != 4:
        print("P2PChat.py <server address> <server port no.> <my port no.>")
        sys.exit(2)
    # get port number for peer connection
    my_port = sys.argv[3]
    connectToRoom()
    win.mainloop()


if __name__ == "__main__":
    main()
